# stack

The Haskell Tool Stack. http://haskellstack.org

[[_TOC_]]

# Official documentation
* [*FAQ*
  ](https://docs.haskellstack.org/en/stable/faq/)

# Templates
* [docs.haskellstack.org](https://docs.haskellstack.org/en/stable/GUIDE/#templates)
* [stack-templates/wiki](https://github.com/commercialhaskell/stack-templates/wiki)

# Commands
## stack config set
* Updates config file.
* May remove commented lines.

### --global
* ~/.stack/config.yaml

## stack exec
* Allows to use Stack outside of a project.

## stack exec env
* Information on paths

## stack ghc
* Allows to use Stack outside of a project.

## stack ghci
* Allows to use Stack outside of a project.

## stack ls stack-colors --basic
* See the current colors sequence.

## stack new
* Creates project Cabal config.

## stack path
* Information on paths

## stack runghc
* Allows to use Stack outside of a project.

## stack setup
* Creates 
  * ~/.stack/global-project/stack.yaml
    * Sets the resolver (default to latest lts).
  * ~/.stack/config.yaml

### Options
#### --system-ghc
* Allows to use use the GHC already on the PATH.

# Configuration files
For more information about stack's configuration, see
* http://docs.haskellstack.org/en/stable/yaml_configuration/

## By project or implicit global project config
This is the implicit global project's config file, which is only used when 'stack' is run outside of a real project.  Settings here do _not_ act as defaults for all projects. To change stack's default settings, edit '~/.stack/config.yaml' instead.
* ~/.stack/global-project/stack.yaml
* Place to set or change the resolver.

## Default settings
This file contains default non-project-specific settings for 'stack', used in all projects.

The following parameters are used by "stack new" to automatically fill fields in the cabal config. We recommend uncommenting them and filling them out if you intend to use 'stack new'.
* See https://docs.haskellstack.org/en/stable/yaml_configuration/#templates
```yaml
templates:
  params:
#    author-name:
#    author-email:
#    copyright:
#    github-username:
```

The following parameter specifies stack's output styles; STYLES is a colon-delimited sequence of key=value, where 'key' is a style name and 'value' is a semicolon-delimited list of 'ANSI' SGR (Select Graphic Rendition) control codes (in decimal). Use "stack ls stack-colors --basic" to see the current sequence.
```yaml
# stack-colors: STYLES
```
* ~/.stack/config.yaml

# See also
* [apk-packages-demo/ghc](https://gitlab.com/apk-packages-demo/ghc)
* [pacman-packages-demo/ghc](https://gitlab.com/pacman-packages-demo/ghc)
